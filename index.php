<?php
$page = 'index';
include('./dev/bootstrap.php');
include(_ROOT_ . 'includes/head.php');
?>
<div class="index">
<?php
$filelist = get_pages_list();
if (!empty($filelist)) :
?>
<dl id="templates">
	<dt><?=((isset($page) && $page == 'index')?'Pages':'TPL')?></dt>
	<dd>
		<ul><?php
		foreach ($filelist as $name => $file) {
			$klass = 'template';
			if (!_DEV_ && $name == 'base') {
				continue;
			}
			echo '<li class="' . $klass . '"><a href="' . url($name) . '">' . $name . '</a></li>' . "\t";
		}
		?></ul>
	</dd>
</dl>
<?php endif; ?>
</div>
<?php include(_ROOT_ . 'includes/foot.php'); ?>