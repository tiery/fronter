<form action="<?=url('homepage', array('s' => 'logged'))?>" method="post" id="user-login" accept-charset="UTF-8">
	<div>
		<div class="form-item form-type-textfield form-item-name">
			<label class=" label-textfield" for="edit-name">E-mail <span class="form-required" title="This field is required.">*</span></label>
			<input required="required" type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required" placeholder="E-mail">
		</div>
		<div class="form-item form-type-password form-item-pass">
			<label class=" label-password" for="edit-pass">Mot de passe <span class="form-required" title="This field is required.">*</span></label>
			<input required="required" type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required" placeholder="Mot de passe">
		</div>
		<input type="hidden" name="form_build_id" value="form-AUKF75Pry16SZrjApdhwO_QUribkieHzJnP6e9QuMh4">
		<input type="hidden" name="form_id" value="user_login">
		<div class="form-actions form-wrapper" id="edit-actions">
			<a href="" class="pass-link">Mot de passe oublié ?</a>
			<input type="submit" id="edit-submit" name="op" value="Valider" class="form-submit">
		</div>
	</div>
</form>