<?php
if ($nid != NULL) :

// Options
$defaults = array(
	'state' => 'default',
	'modifiers' => array()
);
$vars = array_merge($defaults, $options);

// Variables
/*
if (is_object($nid)) {
	$story = $nid;
}
elseif (is_numeric($nid)) {
	$story = node_load($nid);
}
*/

$modifiers = array();
foreach ($vars['modifiers'] as $modifier) {
	array_push($modifiers, 'story-block--' . $modifier);
}
$modifiers = (!empty($modifiers)) ? ' ' . implode(' ', $modifiers) : '';

?>
<div class="story-block<?=$modifiers?>">
	
</div><!-- story-block -->
<?php endif; ?>