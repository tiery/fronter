# Socle intégration

## A propos

Ce socle fournis un environnement de travail et les outils de développement permettant de débuter la phase d'intégration d'un nouveau site. Il contient un ensemble de bonnes pratiques pour optimiser les performances front-end.

L'objectif est de pouvoir présenter l'ensemble des pages HTML correspondant aux maquettes graphiques et de faciliter le branchement dans Drupal.

---

## Prérequis

- [CLI](http://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande) (Teminal, iTerm, ...)
- [Ruby](https://www.ruby-lang.org/fr/) pour faire tourner [Sass](http://sass-lang.com/) (Ruby est déjà installé sur Mac)
- [Compass](http://compass-style.org/) Framework Sass
- Server local avec Apache et le module SSI d'activé sur les fichiers JS ([MAMP](http://www.mamp.info/en/), [WAMP](http://www.wampserver.com/))

---

## Installation

Déposer les fichiers dans un dossier `/integration/` du  répertoire du projet.



Faire pointer le server local sur ce dossier.
Si le root du server ne pointe pas sur le dossier `/integration/`, il faudra modifier la constante `_WEBROOT_` située dans `/dev/config.php`.

**Exemple**

Fichiers sources dans :

`/Users/johndoe/Documents/Sites/[nom_projet]/integration/`

Root server :

`/Users/johndoe/Documents/Sites/[nom_projet]/`

Modifier `_WEBROOT_` :

`/` => `/integration/`

---

## Organisation des fichiers

### Structure
- racine du projet (dans un dossier nommé `/integration/`)
	- .htaccess
	- dev/
	- doc/
	- includes/
	- index.php
	- medias/
	- pages/
	- templates/
	- ui/
		- css/
		- font/
		- img/
		- js/

### /.htaccess
Configuration server inspirée du [htaccess de HTML5Boilerplate](https://github.com/h5bp/html5-boilerplate/blob/master/dist/.htaccess)

### /index.php
Page d'accueil du socle. Affiche la liste les pages disponibles qui est mise à jour automatiquement à partir des fichiers créés dans le dossier `/pages/`

### /dev/
Mini framework PHP : bootstrap, constantes, fonctions d'aides, templates rendering, génération de faux contenu...

### /includes/
Zones communes du site (header, navigation, footer…)

### /medias/
Images de contenu. Elles sont classées par type (news, avatar,…) et nommées par index (0,1,2,3…). Ne pas toucher au dossier `/medias/cache/`.

### /pages/
Pages principales intégrées correspondant aux maquettes.

Nomenclature : `[NNX]_[identifiant-page].php` (nom réservé 000_base.php)

* NN correspond au numéro de page (permet de garder l'ordre des fichiers indépendant de l'ordre alphabétique)
* X correspond à une variation de la page
* L'identifiant est utilisé dans la fonction `url` pour générer le chemin complet d'une page.

Exemple :
* 010-homepage.php
* 011-homepage-membre.php
* 020-article.php
* 021-article-actu.php
* …

### /templates/
Contient les fichiers de template.

### /ui/
Ensemble des fichiers utilisés pour construire l'interface (styles, images de décoration, polices, scripts)

---

## Méthodologie

### 1. Analyse des maquettes
1. Définir les variables de base (taille texte, couleurs principales…)
1. Repérer et classer les éléments spécifiques du design (header, footer, ...) et les modules qui seront réutilisables (boutons, blocs similaires + variations…)
1. Pour chaque élément, définir un nom de base qui sera ensuite utilisé comme class HTML (exemple: `btn`)
1. Définir un nom pour chaque variation de cet élément (exemple `btn--default`)
1. Pour faciliter la compréhension visuelle, il est possible d'exporter une image par élément. Exemple dans le Finder Mac : ![Buttons](assets/buttons.png)

### 2. Intégration des éléments de base

1. Pour chaque élément, créer le fichier `.scss` associé dans le dossier `/ui/scss/base/` ou `/ui/scss/modules/` en fonction de son utilisation. Exemple : `/ui/scss/base/_bouton.scss`
1. Ajouter une référence à ce fichier dans le "master" qui sera compilé `/ui/css/scss/masters/_masters.scss`
1. Dans la page `/pages/000_base.php`, créer un nouveau bloc qui contiendra le HTML de l'élément. On peut écrire directement le HTML :
```
<?=demo_element('Button default', '<a href="" class="btn btn--default">Default button</a>')?>
```
ou utiliser un template :
```
<?=demo_element('Button default', theme('button', array('style' => 'default')))?>
```
Au final on obtient une page de référence des modules ([patterns librairy](http://codepen.io/patterns/)). [Exemple](http://www.palpix.com/interne/legibase/integration/pages/000_base.php).

### 3. Intégration des différentes pages
1. Créer un nouveau fichier dans le dossiers `/pages/` pour chaque gabarit
2. Commencer par la structure principale de la page (exemple : colonnes principale et secondaire)
3. Ensuite intégrer les différents blocs spécifiques de la page

---

## Fonctions PHP

###  - `lorem(type, limit, index)`

**Retour**

{string} Texte de faux contenu.

**Arguments** 

- `type` : {string} type de contenu ('text' par défaut)
- `limit` : {number} nombre de caractères avant de couper le texte ('null' par défaut = le texte n'est pas coupé)
- `index` : {number} index de l'item du tableau du type demandé ('-1' par défaut = index aléatoire)

###  - `media(width, options, is_url_only, src_attr)`

**Retour**

{string} Balise `img` pointant sur une image.

**Arguments**

- `width` : {number} largeur de l'image
- `options` : {array} tableau d'options :
 - `index` : {number} index de l'image du tableau du type de média demandé ('-1' par défaut = index aléatoire)
 - `ratio` : {string} ratio de l'image, doit être déclaré dans le tableau `$ratios`
 - `type` : {string} type du média
 - `crop` : {string} crop de l'image, doit être déclaré dans le tableau `$crops` ('null' par défaut = pas de crop)
 - `retina` : {bool} export pour du retina (quality=30 et dimensions x2)
 - `class` : {string} classes HTML optionnelles à ajouter à la balise `img`
 - `attr` : {string} attributs HTML optionnels à ajouter à la balise `img`
- `is_url_only` : {bool} ne retourne que le chemin de l'image
- `src_attr` : {string} valeur de l'atttribut de la source de l'image ("src" par défaut, peut être "data-src" par exmple si besoin de gérer un chargement conditionnel en javascript)

###  - `url(filename, options)`
**Retour**

{string} Url d'une page.

**Arguments**

- `filename` : {string} identifiant de la page "page-name" dans "NNX_page-name.php" (si vide, récupère la page courrante)
- `options` : {array} tableau permettant de créer la querystring du lien. Non déclaré = récupère la querystring courrante, si chaîne de caractères vide = reset de la querystring courrante. 

**Exemple**
```
print url('home', array('logged'=>1));
// => /pages/010_home.php?logged=1
```

###  - `user_is_logged_in()`
**Retour**

{bool} Renvoie `TRUE` si le visiteur est connecté. Se base sur la constante `_STATUS_` qui récupère le paramètre `s` passé en `GET` dans l'url.

### - `demo_element(name, code, show_code, desc)`

**Retour**

{string} Code HTML de présentation d'un élément de base.

**Arguments**

- `name` : {string} nom de l'élément
- `code` : {string} code HTML de l'élément
- `show_code` : {bool} détermine si le code HTML doit être visible par défaut
- `desc` : {string} description de l'élément

**Exemple**

```
<?=demo_element('Button', '<a href="" class="btn btn--default">Default button</a>')?>
```
sortie HTML : 
```
<div class="demo-wrapper demo-button">
	<span id="demo-button" class="demo-anchor anchor"></span>
	<div class="demo">
		<div class="demo_inner">
			<a href="#demo-button" class="demo__title">Buttons</a>
			<div class="demo__desc"></div>
			<div class="demo__render"><a href="" class="btn btn--default">Default button</a></div>
			<div class="demo__code"><code>&lt;a href="" class="btn btn--default"&gt;Default button&lt;/a&gt;</code></div>
		</div>
	</div>
</div>
```
affichage :

![demo](assets/demo.png)

---

## Styles

### Détail du dossier `/ui/css`

- /ui/css/
	- masters/
	- scss/
		- **base/** : reset CSS, typo, éléments de base...
		- **config/** : déclaration des variables Sass
		- **libraries/** : styles importés depuis une source externe (Drupal,…)
		- **masters/** : fichiers destinés à être compilés
		- **mixins/** : fonctions Sass
		- **modules/** : liste des modules intégrés (header, navigation…). C'est le principal dossier de travail lors de l'intégration.

### Compilation

La configuration Compass se trouve dans le fichier `/ui/config.rb`. Pour lancer la compilation, il suffit d'ouvrir une fenêtre du Terminal, de se déplacer dans le dossier `/ui/` et de lancer cette commande :
```
$ compass compile
```
Pour éviter de lancer cette commande à chaque fois, on peut utiliser la suivante :
```
$ compass watch
```
Cette commande permet de "surveiller" les fichiers et de lancer la compilation à chaque modification.

Les feuilles de style sont générées dans le dossier `/ui/css/masters/` en prenant comme référence le dossier `/ui/css/scss/masters/`. Exemples :

`/ui/css/scss/masters/screen.scss` => `/ui/css/masters/screen.css`

`/ui/css/scss/masters/print.scss` => `/ui/css/masters/print.css`

---

## Aide / FAQ

### Ajouter du faux contenu
Dans `dev/lorem.php`, ajouter un item au tableau `$lorem` : soit un nouveau type de contenu (title, name…), soit un nouveau contenu texte. Exemple :
```
<?php
$lorem = array(
	'firstname' => array('Bob', 'John')
);
?>
```
Utilisation :
```
<?php
print lorem('firstname', 1); // => John
?>
```

### Templating
Le système de templating est basé sur celui de Drupal. Voici les étapes pour créer un nouveau template :

1. Ajouter un item au tableau `$themes` dans `/dev/templates.php`, en se basant sur les élément pré-existants. Exemple :
```
'user_bloc' => array(
		'variables' => array('name' => 'Toto', 'age' => 20),
		'template' => 'user-bloc'
)
```
- `user_bloc` : identifiant du template
- `variables` : valeurs par défaut des variables qui seront injectées dans le fichier de template
- `user-bloc` : nom du ficher de template
2. Dans `/templates/` créer le fichier associé (`user-bloc.tpl.php`) contenant le HTML du template en utilisant les variables passées en paramètres. Exemple :
```
<div class="user-bloc">
		<div>Name: <?=$name?></div>
		<div>Age: <?=$age?></div>
</div>
```

3. Utilisation :
```
<?php
$myage = 30;
print theme('user_bloc', array('age' => $myage));
?>
```
Ce qui affichera :
```
<div class="user-bloc">
		<div>Name: Toto</div>
		<div>Age: 30</div>
</div>
```