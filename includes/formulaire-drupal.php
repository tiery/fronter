<form action="/devel/php" method="post" id="palpix-misc-tiery-test-form" accept-charset="UTF-8">
	<div>
		<div class="form-item form-type-textfield form-item-f0">
			<label for="edit-f0">Champs texte <span class="form-required" title="This field is required.">*</span></label>
			<input type="text" id="edit-f0" name="f0" value="" size="60" maxlength="128" class="form-text required" />
		</div>
		<div class="form-item form-type-checkboxes form-item-f2">
			<label for="edit-f2">Checkbox multiple </label>
			<div id="edit-f2" class="form-checkboxes">
				<div class="form-item form-type-checkbox form-item-f2-SAT">
					<input type="checkbox" id="edit-f2-sat" name="f2[SAT]" value="SAT" class="form-checkbox" />
					<label class="option" for="edit-f2-sat">SAT </label>
				</div>
				<div class="form-item form-type-checkbox form-item-f2-ACT">
					<input type="checkbox" id="edit-f2-act" name="f2[ACT]" value="ACT" class="form-checkbox" />
					<label class="option" for="edit-f2-act">ACT </label>
				</div>
			</div>
		</div>
		<div class="form-item form-type-radios form-item-f3">
			<label for="edit-f3">Radios multiple </label>
			<div id="edit-f3" class="form-radios">
				<div class="form-item form-type-radio form-item-f3">
					<input type="radio" id="edit-f3-0" name="f3" value="0" checked="checked" class="form-radio" />
					<label class="option" for="edit-f3-0">Closed </label>
				</div>
				<div class="form-item form-type-radio form-item-f3">
					<input type="radio" id="edit-f3-1" name="f3" value="1" class="form-radio" />
					<label class="option" for="edit-f3-1">Active </label>
				</div>
				<div class="form-item form-type-radio form-item-f3">
					<input type="radio" id="edit-f3-2" name="f3" value="2" class="form-radio" />
					<label class="option" for="edit-f3-2">Something else </label>
				</div>
			</div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
		</div>
		<div class="form-item form-type-select form-item-f4">
			<label for="edit-f4">Menu select </label>
			<select id="edit-f4" name="f4" class="form-select">
				<option value="0" selected="selected">No</option>
				<option value="1">Yes</option>
				<option value="2">Other</option>
			</select>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
		</div>
		<div class="form-item form-type-textarea form-item-keywords">
			<label for="edit-keywords">Message </label>
			<div class="form-textarea-wrapper resizable">
				<textarea id="edit-keywords" name="keywords" cols="60" rows="5" class="form-textarea"></textarea>
			</div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
		</div>
		<input type="hidden" name="cache-cache" value="ouhhh" />
		<input type="submit" id="edit-edit" name="op" value="Editer" class="form-submit" /><input type="submit" id="edit-delete" name="op" value="Supprimer" class="form-submit" /><div class="container-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit1" name="op" value="Annuler" class="form-submit" /><a href="" class="btn btn--naked">Annuler</a><input type="submit" id="edit-submit2" name="op" value="Envoyer" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-wom8xRxRTfzNkF8U8YMl1vojnrAwyYXojUUV3it827w" />
		<input type="hidden" name="form_token" value="CJWxsA5X9qy8nm1UnD1GwfSZlqGkoU1gf9CCXORZF0Q" />
		<input type="hidden" name="form_id" value="palpix_misc_tiery_test_form" />
	</div>
</form>