<?php
$title = '';
if (isset($page)) {
	$title .= $page . ' - ';
}
$title .= _SITENAME_;
$htmlClasses = 'no-js no-touch';
?>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="fr" class="<?=$htmlClasses?> ie  lte-ie9 lte-ie8 lte-ie7 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="fr" class="<?=$htmlClasses?> ie7 lte-ie9 lte-ie8 lte-ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="fr" class="<?=$htmlClasses?> ie8 lte-ie9 lte-ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="fr" class="<?=$htmlClasses?> ie9 lte-ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="fr" class="<?=$htmlClasses?>"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?=$title?></title>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<!--[if IE 8 ]><link rel="stylesheet" href="<?=_UICSS_?>screen-old-ie.<?=_CACHE_?>.css"><![endif]-->
	<!--[if gt IE 8]><!--><link rel="stylesheet" href="<?=_UICSS_?>screen.<?=_CACHE_?>.css"><!--<![endif]-->
	<link rel="stylesheet" href="<?=_UICSS_?>print.<?=_CACHE_?>.css" media="print">
	<script>
	(function(ua,h,c,t) {
	h[c] = h[c].replace('no-js', 'js');
	var os = ['windows','macintosh','android','iphone','ipad'];
	for (var i=0, l=os.length; i<l; i++) {
		if(new RegExp(os[i]).test(ua)){
			PX_os = os[i];
			h[c] += ' ' + os[i]; break;
		}
	}
	if (!!('ontouchstart' in window) || !!(window.DocumentTouch && document instanceof DocumentTouch)) {
		h[c] = h[c].replace('no-' + t, t);
	}
	})(navigator.userAgent.toLowerCase(), document.documentElement, 'className', 'touch');
	var PX = <?=json_encode($PX)?>;
	</script>
	<link rel="shortcut icon" href="<?=_WEBROOT_?>favicon.ico">
</head>
<?php
$body_attr = '';
$body_class = array('page-' . $page);
if (_STATUS_ != '') {
	array_push($body_class, _STATUS_);
}
$body_class = implode(' ', $body_class);
if ($body_class != '') {
	$body_attr .= ' class="' . $body_class . '"';
}
?>
<body <?=$body_attr?>>
<div class="site" id="site">