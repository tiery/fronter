<form class="search-form" action="/" method="post" id="search-block-form--3" accept-charset="UTF-8">
	<div>
		<div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
			<div class="form-item form-type-textfield form-item-search-block-form">
				<label class="element-invisible has-placeholder label-textfield" for="edit-search-block-form--6">Search </label>
				<input title="Enter the terms you wish to search for." placeholder="Mariage, dossier, naissance..." type="text" id="edit-search-block-form--6" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
			</div>
			<div class="form-actions form-wrapper" id="edit-actions--3">
				<input class="form-submit" type="submit" id="edit-submit--3" name="op" value="Rechercher" />
			</div>
			<input type="hidden" name="form_build_id" value="form-kT4p7ti7ElbPm_G8uIgTs9GBSw-V3ElzRHhK5hW9RK4" />
			<input type="hidden" name="form_token" value="ckJsFWGWiHVCgofR3GMlg2ZdHyMBWL0isXAOOYp9xGE" />
			<input type="hidden" name="form_id" value="search_block_form" />
		</div>
	</div>
</form><!-- search-form -->