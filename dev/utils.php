<?php

function get_rand_int ($max = 50, $min = 0) {
	return rand($min, $max);
}

function clean_str ($str, $length = 80) {
	static $_search = array(' ');
	static $_replace = array(' ');
	if(is_string($str)){
		return substr(trim(strip_tags(str_replace($_search, $_replace, $str))), 0, $length);
		//return truncate_utf8(trim(strip_tags(str_replace($_search, $_replace, $str))), $length, TRUE, TRUE);
	}
}

// ex : lorem('people', null, 2)
// 3e élément du tableau de contenu de type people sans limite de caractère
function lorem ($type = 'text', $limit = null, $index = -1) {
	global $lorem;
	$returnValue = '';
	$contens_type = $lorem[$type];
	if ($index == -1) {
		$index = rand(0, count($contens_type)-1);
	}
	$returnValue = $lorem[$type][$index];
	if ($limit != null) {
		$returnValue = clean_str($returnValue, $limit) . '...';
	}
	return $returnValue;
}

// ex : lorem_render(5, 'p', 'people')
// 5 paragraphes avec du contenu de type people
function lorem_render ($n = 1, $tag = 'p', $type = 'text') {
	$output = '';
	while ($n--) {
		$output .= '<' . $tag . '>' . lorem($type) . '</' . $tag . '>';
	}
	return $output;
}

// ex : media_get_type('actu')
// retourne la liste des fichiers présents dans /medias/actu/
function media_get_type ($type = 'actu') {
	$media_dir = _ROOT_ . 'medias/' . $type . '/';
	$filelist = array();
	if ($dir = @opendir($media_dir)) {
		while (($file = readdir($dir)) !== false) {
			if($file != ".." && $file != "." && $file != ".DS_Store") {
				$filelist[] = $file;
			}
		}
		closedir($dir);
	}
	return $filelist;
}

// ex : media(960, array('index'=>2, 'ratio'=>'l', 'crop'=>'r'), false, $src_attr)
function media ($width = null, $options = array(), $is_url_only = false, $src_attr = 'src') {
	$output = '';
	
	// Settings
	$q = 90;
	$ratios = array(
		'c'   => 1,
		'p'   => 2/3,
		'l'   => 22/13,
	);
	$crops = array('c', 't', 'l', 'r', 'b', 'tl', 'tr', 'bl', 'br');
	
	// Options
	$defaults = array(
		'index' => -1,  // nom du fichier de média
		'ratio' => 'l',   // nom du ratio
		'type'  => 'news',// type de contenu
		'crop'  => null,  // origine du crop (center, top,...)
		'retina'=> false, // gestion du retina => quality=30 et dimensions x2
		'class' => '',    // class à ajouter
		'attr'  => ''     // attributs à ajouter
	);
	
	$o = array_merge($defaults, $options);
	
	// Process
	$files = media_get_type($o['type']);
	
	if ($files) {
		if ($o['index'] === -1 || gettype($o['index']) != 'integer') {
			$index = rand(0, max(array_keys($files)));
		}
		else {
			$index = $o['index'];
		}
		
		if ($width != null) {
			$c = ($o['crop'] == null) ? $crops[0] : $o['crop'];
			$height = floor($width * 1/$ratios[$o['ratio']]);
			if ($o['retina']) {
				$q = 30;
				$srcHeight = $height * 2;
				$srcWidth = $width * 2;
			}
			else {
				$srcHeight = $height;
				$srcWidth = $width;
			}
			$src = _MEDIAS_ . 'timthumb.php?src=' . _MEDIAS_ . $o['type'] . '/' . $index . '.jpg&q=' . $q . '&w=' . $srcWidth . '&h=' . $srcHeight . '&a=' . $c;
		}
		else {
			$src = _MEDIAS_ . $o['type'] . '/' . $index . '.jpg';
			$infos = getimagesize(_ROOT_ . $src);
			$width = $infos[0];
			$height = $infos[1];
		}
		
		if ($is_url_only) {
			$output = $src;
		}
		else {
			$class_attr = (!empty($o['class'])) ? 'class="' . $o['class'] . '" ' : '';
			$output = '<img ' . $src_attr . '="' . $src . '" width="' . $width . '" height="' . $height . '" '. $class_attr . $o['attr'] . '>';
		}
	}
	
	return $output;
}

function get_pages_list () {
	$filelist = array();
	if ($dir = @opendir(_ROOT_ . 'pages/')) {
		while (($file = readdir($dir)) !== false) {
			if($file != ".." && $file != "." && $file != ".DS_Store" && $file != 'index.php') {
				$filelist[get_page_name($file)] = $file;
			}
		}
		closedir($dir);
	}
	return $filelist;
}

function get_page_name ($page) {
	$count = count($page) - 5;
	$underscore_pos = strpos($page, '_') + 1;
	$name = substr($page, $underscore_pos, $count);
	return $name;
}

// retourne une chaine de caractère "machine readable"
function urlify ($str) {
	return strtolower(preg_replace('/([^.a-z0-9]+)/i', '', $str));
}

// retourne l'url d'un template avec une querystring en option
function url ($filename = null, $options = array()) {
	$defaults = empty($_GET) ? array() : $_GET;
	$config = array();
	if (gettype($options) != "array") {
		$config = null;
	}
	else if (is_array($defaults)) {
		$config = array_merge($defaults, $options);
	}
	else {
		$config = $options;
	}
	$querystring = '';
	if ($config != null && is_array($config) && !empty($config)) {
		$querystring = '?' . http_build_query($config);
	}

	if ($filename == '') {
		$path = $_SERVER['PHP_SELF'];
	}
	elseif ($filename == 'root') {
		$path = _WEBROOT_;
	}
	else {
		$pages = get_pages_list();
		$path = _PAGES_;
		if (array_key_exists($filename, $pages)) {
			$path .= $pages[$filename];
		}
		else {
			$path .= $filename . '.php';
		}
	}
	$url = $path . $querystring;

	return $url;
}

// demo element wrapper
function demo_element ($name, $code, $show_code = true, $desc = null) {
	$code = trim($code);
	$id = urlify($name);
	$html = '<div class="demo-wrapper demo-' . $id . '">
	<span id="demo-' . $id . '" class="demo-anchor anchor"></span>
	<div class="demo">
		<div class="demo_inner">
			<a href="#demo-' . $id . '" class="demo__title">{{name}}</a>
			<div class="demo__desc">{{desc}}</div>
			<div class="demo__render">{{render}}</div>
			<div class="demo__code"><code>{{code}}</code></div>
		</div>
	</div>
</div><!-- demo-' . $id . ' -->' . "\n";
	$html = str_replace('{{name}}', $name, $html);
	$html = str_replace('{{desc}}', $desc, $html);
	$html = str_replace('{{render}}', $code, $html);
	if ($show_code) {
		$html = str_replace('{{code}}', htmlspecialchars($code), $html);
	}
	else {
		$html = str_replace('<pre class="demo__code">{{code}}</pre>', '', $html);
	}
	return $html;	
}

// Retourne une date aléatoire
// entre le 1er janvier de l'année en cours et maintenant
// avec le format demandé
// + traduction
function rand_date ($format = 'Y-m-d H:i:s') {
	$en = array(
		'days' => array (
			'monday',
			'tuesday',
			'wednesday',
			'thursday',
			'friday',
			'saturday',
			'sunday'
		),
		'months' => array (
			'january',
			'february',
			'march',
			'april',
			'may',
			'june',
			'july',
			'august',
			'september',
			'october',
			'november',
			'december'
		)
	);
	
	$fr = array(
		'days' => array (
			'lundi',
			'mardi',
			'mercredi',
			'jeudi',
			'vendredi',
			'samedi',
			'dimanche'
		),
		'months' => array (
			'janvier',
			'février',
			'mars',
			'avril',
			'mai',
			'juin',
			'juillet',
			'août',
			'septembre',
			'octobre',
			'novembre',
			'décembre'
		)
	);
	
	$min = mktime(0, 0, 0, 1, 1, date('Y')); 
	$max = time();
	$timestamp = rand($min, $max);
	$date = date($format, $timestamp);
	$date = str_ireplace($en['days'], $fr['days'], $date);
	$date = str_ireplace($en['months'], $fr['months'], $date);
	return $date;
}

function user_is_logged_in () {
	return _STATUS_ == 'logged';
}