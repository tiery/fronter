<?php

$themes = array(
	'snippet' => array(
		'variables' => array('data' => array(), 'options' => array()),
		'template' => 'snippet'
	),
	'login_form' => array(
		'variables' => array(),
		'template' => 'login-form'
	),
	'breadcrumb' => array(
		'variables' => array('elements' => array()),
		'template' => 'breadcrumb'
	)
);

function theme_render_template ($template_file, $variables) {
  // Extract the variables to a local namespace
  extract($variables, EXTR_SKIP);
	// Start output buffering
  ob_start();
	// Include the template file
  include $template_file;
	// End buffering and return its contents
  return ob_get_clean();
}

function theme ($hook, $variables = array()) {
	global $themes;
	$output = '';
	if (isset($themes[$hook])) {
		$theme = $themes[$hook];
		$theme_template = _ROOT_ . 'templates/' . $theme['template'] . '.tpl.php';
		$variables += $theme['variables'];
		$output = theme_render_template($theme_template, $variables);
	}
	else {
		$output = '<code><< Erreur de templating (theme: ' . $hook . ') >></code>';
	}
	return $output;
}
?>