<?php
// Site Name
define('_SITENAME_', 'Fronter');

// Working directories
define('_WEBROOT_', '/');
define('_ROOT_', $_SERVER['DOCUMENT_ROOT'] . _WEBROOT_);

// Dev mode
// Auto true if local IP
$dev = strpos($_SERVER['HTTP_HOST'], '192.168') !== FALSE || $_SERVER['SERVER_NAME'] == 'localhost';
define('_DEV_', $dev);

// Status
$status = 'anonym';
if (isset($_GET['s']) && !empty($_GET['s'])) {
	$status = $_GET['s'];
}
define('_STATUS_', $status);

if (_WEBROOT_ == '' || _SITENAME_ == '') {
  echo '<h1>Votre site n\'est pas correctement configuré.</h1>';
  echo '<p>Veuillez mettre à jour de fichier config.php situé à la racine du site.</p>';
  exit;
}

// Assets Cache buster
define('_CACHE_', time());

// Current page path
define('_PAGE_', $_SERVER["REQUEST_URI"]);

// Theme directory
define('_UICSS_', _WEBROOT_ . 'ui/css/masters/');
define('_UIIMG_', _WEBROOT_ . 'ui/img/');
define('_UIJS_', _WEBROOT_ . 'ui/js/');

// Templates directory
define('_TPL_', _WEBROOT_ . 'templates/');
define('_PAGES_', _WEBROOT_ . 'pages/');
define('_INC_', _ROOT_ . 'includes/');

// Medias directory
define('_MEDIAS_', _WEBROOT_ . 'medias/');
define('_TRASH_', _WEBROOT_ . 'trash/');

// PX javascript global object
$PX = array(
	'config' => array(
		'cacheQuery' => _CACHE_,
		'devMode' => _DEV_
	),
	'path' => array(
		'webroot' => _WEBROOT_,
		'scripts' => _UIJS_
	)
);
?>
