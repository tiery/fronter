<?php $page = 'base'; ?>
<?php include('../dev/bootstrap.php');  ?>
<?php include(_ROOT_ . 'includes/head.php'); ?>

<div class="trunk">
	<?=demo_element('Breadcrumb', theme('breadcrumb', array('elements' => array('Accueil', 'Outils', 'Lexique'))))?>
	<?=demo_element('Base', '
		<div class="h1">Titre de niveau 1</div>
		<p>Lorem ipsum dolor sit amet, <a href="">consectetur</a> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<div class="h2">Titre de niveau 2</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<div class="h3">Titre de niveau 3</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	')?>
	<?=demo_element('Buttons', '
	<a href="'.url('00_base').'" class="button">Default button</a>
	<a href="'.url('00_base').'" class="btn btn--naked">Naked button</a>
	<a href="'.url('00_base').'" class="btn btn--mute">Mute button</a>
	')?>
	<?=demo_element('Pagination', file_get_contents(_ROOT_ . 'includes/pagine.php'))?>
	<?=demo_element('Grid', '<div class="grid">
		<div class="grid_row">
			<div class="grid_col-6-12"><div class="tc" style="background-color:#f5f5f5;">1/2</div></div>
			<div class="grid_col-6-12"><div class="tc" style="background-color:#f5f5f5;">1/2</div></div>
		</div>
		<div class="grid_row grid_row--sep">
			<div class="grid_col-4-12"><div class="tc" style="background-color:#f5f5f5;">1/3</div></div>
			<div class="grid_col-4-12"><div class="tc" style="background-color:#f5f5f5;">1/3</div></div>
			<div class="grid_col-4-12"><div class="tc" style="background-color:#f5f5f5;">1/3</div></div>
		</div>
		<div class="grid_row">
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
			<div class="grid_col-2-12"><div class="tc" style="background-color:#f5f5f5;">1/6</div></div>
		</div>
		<div class="grid_row grid_row--sep">
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
			<div class="grid_col-1-12"><div class="tc" style="background-color:#f5f5f5;">1/12</div></div>
		</div>
	</div>')?>
</div>

<?php include(_ROOT_ . 'includes/foot.php'); ?>