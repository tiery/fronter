(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'switcher',
		defaults = {
			handler_class: false,
			event: 'click'
		},
		base_class = pluginName,
		handler_class = base_class + '_handler',
		open_class = base_class + '--open',
		Plugin;

	function close ($elements) {
		$elements.each(function(i, element){
			var $element = $(element);
			if ($element.hasClass(open_class)) {
				$element.removeClass(open_class);
				$(window).trigger('SWITCHER_CLOSE', element);
			}
		});
	};
	
	function open ($elements) {
		$elements.each(function(i, element){
			var $element = $(element);
			if (!$element.hasClass(open_class)) {
				$element.addClass(open_class);
				$(window).trigger('SWITCHER_OPEN', element);
			}
		});
	};
	
	function toggle ($elements) {
		$elements.each(function(i, element){
			var $element = $(element);
			if ($element.hasClass(open_class)) {
				close($element);
			}
			else {
				open($element);
			}
		});
	};

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		/* window[pluginName + i] = this; */

		// Main element
		this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

		// Options
		var elementOptions = this.$element.data(pluginName);
		var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default
		
		// DOM elements
		// this.$inner	 = $('.une__inner', element);
		
		// Properties
		this.instance = pluginName + '_' + i;

		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		
		// Events init
		that.events();
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this,
			handler_element_class = that.opts.handler_class;
		
		// Add classes
		that.$element.addClass(pluginName);	
		if (handler_class) {
			that.$handler = that.$element.find('.' + handler_element_class);
		}
		else {
			that.$handler = $('> *:eq(0)', that.$element);
		}
		that.$handler.addClass(handler_class);
		
		// Events binding
		if (PX.test('touch') || that.opts.event === 'click') {
			that.$element.data('listen_click', true);
		}
		else {
			that.bind_rollover();
		}
		
		// Event triggering
		that.$element.on(pluginName + '_open', function(){
			open(that.$element);
		});
		that.$element.on(pluginName + '_close', function(){
			close(that.$element);
		});
		that.$element.on(pluginName + '_toggle', function(){
			toggle(that.$element);
		});
	};
	
	Plugin.fn.bind_rollover = function () {
		var that = this,
			delay = 150,
			timer_over,
			timer_out
		
		that.$element.mouseover(function(){
			clearTimeout(timer_out);
			timer_over = setTimeout(function(){
				clearTimeout(timer_over);
				open(that.$element);
			}, delay);
		}).mouseout(function(){
			clearTimeout(timer_over);
			timer_out = setTimeout(function(){
				clearTimeout(timer_out);
				close(that.$element);
			}, delay);
		});
	};
	
	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};
	
	$(document).on('click.' + pluginName, function(e){
		var $target = $(e.target),
			$handler,
			$switcher,
			$swicther_to_close,
			handler_selector = '.' + handler_class,
			switcher_selector = '.' + base_class;
	
		// Click sur/dans un switcher ?
		if ($target.hasClass(base_class)) {
			$switcher = $target.hasClass(base_class);
		}
		else if ($target.parents(switcher_selector).length) {
			$switcher = $target.parents(switcher_selector);
		}
		
		// Click sur/dans un switcher qui réagit au click ?
		if ($switcher){
			// On recherche si on a cliqué sur le handler
			if ($target.hasClass(handler_class)) {
				$handler = $target;
			}
			else if ($target.parents(handler_selector).length) {
				$handler = $target.parents(handler_selector);
			}
			if ($handler && $switcher.data('listen_click')) {
				$switcher = $handler.parents(switcher_selector);
				toggle($switcher);
				$swicther_to_close = $(switcher_selector).filter(function(){
					return this !== $switcher[0];
				});
			}
		}
		else {
			$swicther_to_close = $(switcher_selector);
		}
		
		if ($swicther_to_close) {
			close($swicther_to_close);
		}
	});
	
	$(document).on('click', '.switcher_closer', function(){
		var $closer = $(this);
		$closer.parents('.' + base_class + ':eq(0)').trigger(pluginName + '_close');
	});
	

}(jQuery));
