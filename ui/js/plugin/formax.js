<!--#include virtual="formax-presets.js"-->

(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'formax',
		defaults = {
			ajax: true,
			ajaxUrl: '',
			ajaxDataType: 'jsonp',
			handleMessages: true,
			onBefore: function(){},
			onSubmit: function(){},
			onAjaxSubmit: function(){},
			onFail: function(){}, // erreur ajax (!= erreur de données du formulaire)
			onDone: function(){}, // success ajax
			onStatus: function(){}, // success ajax, success validation
			onError: function(){}, // success ajax, error validation
			onAlways: function(){} // complete ajax
		},
		keys = {
			loading: pluginName + '--loading'
		},
		Plugin;

	/*
	 * Helpers
	 */	
	function ns (str, sep) { // Namespacer
		var separator = sep || '_';
		return [pluginName, separator, str].join('');
	}
	
	function fadeRemove ($el, cb) {
		$el.fadeOut(function(){
			$(this).remove();		
		});
		typeof cb === 'function' && cb();
	}
	
	function handleMessages ($wrapper, cb) {
		var timer = setTimeout(function() {
			clearTimeout(timer);
			fadeRemove($messages, cb);
		}, 5000);
		var $messages = $wrapper.find('.messages').on('click', function(){
			clearTimeout(timer);
			fadeRemove($messages, cb);
		});
	}
	
	function renderMessage (message, type) {
		var type = type || 'status';
		return '<div class="messages ' + type + '">' + message + '</div>';
	}

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		window[pluginName + i] = this;

		// Main element
		this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

		// Options
		var elementOptions = this.$element.data(pluginName),
			presetName,
			opts;
		
		if (typeof elementOptions === 'string' && PX.plugins.list[pluginName].presets && PX.plugins.list[pluginName].presets[elementOptions]) { // preset
			presetName = elementOptions;
			elementOptions = PX.plugins.list[pluginName].presets[presetName];
			elementOptions.preset = presetName;
		}
		opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default

		// DOM elements
		// this.$inner	 = $('.une__inner', element);
		
		// Properties
		this.status = 'wait'; // wait | busy
		this.timer;
		
		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		// Events init
		that.events();
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this;

		that.$element.on('submit', function(e) {
			// Prevent multiple submit
			if (that.status === 'busy') {
				return;
			}
			that.status = 'busy';
			
			// Change form state
			that.timer = setTimeout(function(){
				clearTimeout(that.timer);
				that.$element.addClass(keys.loading);
			}, 50);
			
			// trigger onSubmit hook
			that.opts.onSubmit.call(that);
			
			// Ajax mode 
			if (that.opts.ajax) {
				e.preventDefault();
				that.ajaxSubmit();
			}
		});
	};

	/*
	 * ajaxSubmit
	 */
	Plugin.fn.ajaxSubmit = function () {
		var that = this,
			ajaxConfig = {
				url: that.opts.ajaxUrl,
				dataType: that.opts.ajaxDataType
			},
			$req,
			$wrapper = that.$element.parents('.formax-wrapper:eq(0)');
		
		// trigger onAjaxSubmit hook
		that.opts.onAjaxSubmit.call(that);
		
		// Complete Ajax config
		if (!ajaxConfig.url) {
			ajaxConfig.url = that.$element.attr('action');
		}
		if (!ajaxConfig.dataType) {
			ajaxConfig.dataType = 'jsonp';
		}
		ajaxConfig.data = that.$element.serialize();

		// Send request
		$req = $.ajax(ajaxConfig);
		
		$req.fail(function() {
			// trigger onFail hook
			that.opts.onFail.apply(that, arguments);
			if (that.opts.handleMessages) {
				$wrapper.find('.messages').remove();
				$wrapper.prepend(renderMessage('Une erreur est suvenue, merci de réessayer ultérieurement.', 'error'));
				handleMessages($wrapper);
			}
			PX.utils.log('Formax fail request', arguments);
		});
		
		$req.done(function(res) {
			var args = arguments,
				status = (res && res.status) || 'unknown',
				$wrapper = that.$element.parents('.formax-wrapper:eq(0)'),
				$messages,
				timer;
			
			// trigger onDone hook
			that.opts.onDone.apply(that, args);
			
			if (status === 'unknown') {
				if (that.opts.handleMessages) {
					$wrapper.find('.messages').remove();
					$wrapper.prepend(renderMessage('Une erreur est suvenue, merci de réessayer ultérieurement.', 'error'));
				}
				PX.utils.log('Formax unknown status', res);
			}
			else {
				if (status === 'status') {
					// trigger onStatus hook
					that.opts.onStatus.apply(that, args);
				}
				else if (status === 'error') {
					// trigger onError hook
					that.opts.onError.apply(that, args);
				}
				if (that.opts.handleMessages) {
					if (typeof res.message === 'string') { // si message = string, on suppose que c'est du html
						$wrapper.html(res.message).find('form').formax();
					}
				}
			}
			if (that.opts.handleMessages) {
				handleMessages($wrapper);
			}
		});
		
		$req.always(function(res) {
			// Change form state
			clearTimeout(that.timer);
			that.$element.removeClass(keys.loading);
			that.status = 'wait';
			// trigger onAlways hook
			that.opts.onAlways.apply(that);
			// Debug
			PX.utils.log('Formax debug', (res.debug ? res.debug : res));
		});
		
	};

	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};
	

}(jQuery));