/*
 * Select plugin
 */
(function ($) {

    'use strict';

    // Params
    var pluginName = 'select',
        defaults = {
	        sClass: 'select',
	        sSelected: 'selected',
	        sFocus: 'focus',
            fixed: false
        },
        
    // Template
        tpl = '\
    	<dl>\
    		<dt><span class="action"><i></i></span><div class="text"></div></dt>\
    		<dd><ul></ul></dd>\
    	</dl>';

    // Plugin constructor
    var Plugin = function (element, options, i) {
        
        // Plugin exposed to window
        window[pluginName + i] = this;
        
        // DOM elements
        this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');
        this.$items = $('> *', this.$element);
        this.$selected = this.$element.find('option').filter(':eq(' + this.$element[0].selectedIndex + ')');
        
        // Options
        var opts  = $.extend({}, options, this.$element.attr('data-' + pluginName + '-options')); // element options -> plugin init options
        this.opts = $.extend({}, defaults, opts); // options -> default
        if (element.id === 'edit-term') {
            this.opts.fixed = true;
        }
        
        // Go!
        this.init();
    };
    
    // Shortcut for Plugin object prototype
    Plugin.fn = Plugin.prototype;
    
    // Initialization logic
    Plugin.fn.init = function () {
        var that = this;
        this.$custom = $(tpl).addClass(that.opts.sClass).attr('id', pluginName.toLowerCase() + '-' + that.$element[0].id);
        this.$header = this.$custom.find('dt');
        
        // Create html
        that.create();
        
        // Events init
        that.events();
    };
    
    // Create
    Plugin.fn.create = function () {
        var that = this,
            klass,
        	$item,
        	output = [];
        	
        var populate = function ($items, indented) {
	        $.each($items, function(){
	        	$item = $(this);
	        	klass = '';
		        if (this.nodeName.toLowerCase() === 'optgroup') {
			        output.push('<li class="label">' + $item.attr('label') + '</li>');
			        populate($('> *', this), true);
		        }
		        else {
		        	if ($item.attr('value') === that.$selected.attr('value')) {
			        	klass += ' selected';
		        	}
		        	if (indented) {
			        	klass += ' indented';
		        	}
		        	output.push('<li class="' + klass + '" data-value="' + $(this).attr('value') + '">' + $(this).text() + '</li>');
		        }
	        });
        };
        
        populate(that.$items);
        
        that.$custom
        	.find('.text').text(that.$selected.text()).end()
        	.find('ul').html(output.join(''));
        
        that.$element.after(that.$custom);
        if (that.opts.fixed) {
            that.$custom.width(that.$custom.width() + 10);
        }
        else {
            //that.$custom.css('min-width', that.$custom.width() + 10);
        }
    };
    
    // Evenements
    Plugin.fn.events = function () {
        var that = this;
        
        // Select item
        that.$custom.find('li').click(function(){
        	var $handler = $(this).addClass(that.opts.sFocus);
        	var updateTimer = setTimeout(function(){
        		clearTimeout(updateTimer);
	        	that.update($handler.removeClass(that.opts.sFocus));
        	}, 150);
        });
        
        // Open/Close select
        that.$header.click(function(){
	        if (that.$custom.hasClass('open')) {
		        that.$custom.removeClass('open');
	        }
	        else {
	        	$('.' + that.opts.sClass).removeClass('open');
		        that.$custom.addClass('open');
	        }
        });
        
        // Click anywhere else
        $(document).click(function(e){
	        var $target = $(e.target);
	        if (!$target.hasClass(that.opts.sClass) && !$target.parents().hasClass(that.opts.sClass)) {
		        $('.' + that.opts.sClass).removeClass('open');
	        }
        });
        
        // Synchronyze "select change" to "custom update"
        that.$element.on('change', function(e){
	        var newVal = that.$element.val();
	        var $handler = that.$custom.find('li[data-value="' + newVal + '"]');
	        that.update($handler, true);
        });
    };
    
    // Update select
    Plugin.fn.update = function (handler, force) {
        var that = this,
        	value = $(handler).attr('data-value');
        if (value != that.$element.val() || force) {
	        that.$element.val(value);
	        that.$custom
	        	.find('.selected').removeClass('selected').end()
	        	.find('.text').text( $(handler).addClass('selected').text() );	        
	        that.$element.trigger('update', { value: value });
        }
        that.close();
    };
    
    // Close
    Plugin.fn.close = function () {
        var that = this;
        this.$custom.removeClass('open');
    };
    
    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

}(jQuery));