(function($) {

var pluginName = 'popin',
	plugin = PX.plugins.list[pluginName],
	presets = plugin.presets = {},
	popin = document.getElementById('colorbox');

plugin.init = function (context) {
	if (context) {
		if (context.nodeType) {
			$(plugin.selector, context).popin();
		}
		else if (typeof context === 'function') {
			context();
		}
	}
	else {
		$(plugin.selector, document).popin();
	}
};

presets['user-login'] = {
	href: '/ajax/form/user_login',
	width: 500,
	onComplete: function () {
		plugin.init(popin);
	}
};

presets['user-pass'] = {
	href: '/ajax/form/user_pass',
	width: 500,
	onComplete: function () {
		plugin.init(popin);
	}
};

}(jQuery));
