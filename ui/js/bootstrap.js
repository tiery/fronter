<!--#include virtual="libraries/fastclick.js"-->
<!--#include virtual="libraries/yepnope.js"-->
<!--#include virtual="libraries/matchMedia.js"-->
<!--#include virtual="libraries/jquery.js"-->
<!--#include virtual="libraries/base64.js"-->
<!--#include virtual="palpix/base.js"-->
<!--#include virtual="palpix/test.js"-->
<!--#include virtual="palpix/plugins.js"-->
<!--#include virtual="plugin/l.js"-->

/*
 * Specific Touch device behavior
 */
(function(h,c,t) {
	if (PX.test(t)) {
		// Add FastClick
		window.addEventListener('load', function() {
		    FastClick.attach(document.body);
		}, false);
	}
})(document.documentElement, 'className', 'touch');

/*
 * Plugins initialization
 */
(function($) {

	// Fake linker
	$('[data-l]').pxL();

	// Dev
	PX.plugins.register({
		name: 'dev',
		condition: function () {
			return PX.config.devMode;
		}
	});
	
	// Switcher
	PX.plugins.register({
		name: 'switcher',
		condition: function () {
			return true;
		},
		callback: function () {
			$('.dropdown').switcher({
				handler_class: 'dropdown_handler',
				event: 'rollover'
			});
			$('.tool--has-content').switcher({
				handler_class: 'tool_handler'
			});
		}
	});
	
	// Custom selects
	/*PX.plugins.register({
		name: 'select',
		condition: function () {
			return document.getElementsByTagName('select') && !$(document.documentElement).hasClass('lte-ie8');
		},
		callback: function () {
			$(document.getElementsByTagName('select')).select();
		}
	});*/
	
}(jQuery));
