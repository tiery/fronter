/*
 * PX definintion & settings
 */	
(function() {

	var PX = window.PX || {};
	
	// Configuration variables (should be intializing in site context)
	PX.config = PX.config || {};
	
	// Ads variables (should be intializing in site context)
	PX.ads = PX.ads || {};
	
	// Ads variables (should be intializing in site context)
	PX.broadcasts = PX.broadcasts || {};
	
	// Debug mode
	PX.debug = !!(window.location.href.indexOf('pxdebug') > -1);
	
	// Expose PX to context
	window.PX = PX;
	
})();

/*
 * PX.utils / PXU
 * Toolbox
 */
(function() {
	
	var breaker = {},
		has = function(obj, key) {
			return hasOwnProperty.call(obj, key);
		};
	
	PX.utils = {
	
		// an 'each' implementation, aka 'forEach'
		each: function(obj, iterator, context) {
			if (obj == null) return;
			if (Array.prototype.forEach && obj.forEach === Array.prototype.forEach) {
				obj.forEach(iterator, context);
			}
			else if (obj.length === +obj.length) {
				for (var i = 0, l = obj.length; i < l; i++) {
					if (iterator.call(context, obj[i], i, obj) === breaker) return;
				}
			}
			else {
				for (var key in obj) {
					if (has(obj, key)) {
						if (iterator.call(context, obj[key], key, obj) === breaker) return;
					}
				}
			}
		},
		
		// Extend a given object with all the properties in passed-in object(s)
		extend: function (obj) {
			PXU.each(Array.prototype.slice.call(arguments, 1), function(source) {
		    	if (source) {
			    	for (var prop in source) {
				    	obj[prop] = source[prop];
				    }
				}
			});
		    return obj;
		},
		
		// Check if variable is an array
		isArray: function (arr) {
			return !!(Object.prototype.toString.call(arr) === '[object Array]');
		},
		
		// Search an item in an array
		inArray: function(find, arr) {
			var i;
			if (arr && this.isArray(arr)) {
				i = arr.length;
				while (i--) {
					if (i in arr && arr[i] === find) {
						return i;
					}
				}
			}
			return -1;
		},
		
		// Custom log
		log: function () {
			if (PX.debug && window.console && window.console.log) {
				console.log(Array.prototype.slice.call(arguments));
			}
		},
		
		// Find & Replace
		findReplace: function (str, find, replace) {
			var reg = new RegExp(find, 'g');
			return str.replace(reg, replace);
		},
		
		// Window open
		openWin: function (url, name, options) {
			var screenHeight = screen.height;
			var screenWidth = screen.width;
			var defaults = {
				width: 600,
				height: 400,
				resizable: 'yes',
				scrollbars: 'yes',
				toolbar: 'no',
				location: 'no',
				directories: 'no',
				status: 'no',
				menubar: 'no',
				copyhistory: 'no',
				top: 0,
				left: 0
			};
			var o = PXU.extend(defaults, options);
			var str = [];
			str.push('width=' + o.width);
			str.push('height=' + o.height);
			str.push('resizable=' + o.resizable);
			str.push('scrollbars=' + o.scrollbars);
			str.push('toolbar=' + o.toolbar);
			str.push('location=' + o.location);
			str.push('directories=' + o.directories);
			str.push('status=' + o.status);
			str.push('menubar=' + o.menubar);
			str.push('copyhistory=' + o.copyhistory);
			str.push('top=' + ((screenHeight/2)-(o.height/2)));
			str.push('left=' + ((screenWidth/2)-(o.width/2)));
			str = str.join(',');
			window.open(url, name, str);
		}
	};
	
	// Shortcut => PX.utils.foo() === PXU.foo()
	window.PXU = PX.utils;
	
})();

/*
 * PX.mod
 * Modules dependency management
 */	
(function() {
	
	// Loaded modules list
	var _list = [];
	
	// Modules dependency object
	PX.mod = {
	
		// Test
		test: function (defined, required) {
			var bool;
			if (arguments.length > 0) {
				bool = this.define(defined);
			}
			else if (bool && required) {
				bool = this.require(required);
			}
			return bool;
		},
	
		// Module loading test
		isLoaded: function (module) {
			return !!(PXU.inArray(module, _list) > -1);
		},
		
		// Define new module
		define: function (module) {
			if (this.isLoaded(module)) {
				PXU.log('Module déjà chargé: px.' + module, 'error');
				return;
			}
			_list.push(module);
			return true;
		},
		
		// Wrapper for modules testing
		require: function (modules) {
			var i,
				ref,
				res,
				bool = true,
				error = function () {
					PXU.log('Module requis: "' + ref + '"', 'error');
				};
			if (!modules) {
				return;
			}
			if (typeof modules === 'string') {
				ref = modules;
				res = this.isLoaded(ref);
				if (!res) {
					error();
				}
			}
			if (PXU.isArray(modules) && modules.length > 0) {
				if (modules.length === 1) {
					ref = modules[0];
					res = this.isLoaded(ref);
					if (!res) {
						error();
					}
				}
				else {
					i = modules.length;
					while (i--) {
						ref = modules[i];
						res = this.isLoaded(ref);
						if (!res) {
							bool = false;
							error();
						}
					}
					res = bool;
				}
			}
			return res;
		}
	};
	
})();
