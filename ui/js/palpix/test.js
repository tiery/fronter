/*
 * Palpix testing module
 * for conditional loader
 *
 * PX.test(type, data);
 * 
 * PX.test('rwd'); // return string (desktop|tablet|moblet|mobile)
 * PX.test('rwd', ['desktop', 'tablet', 'moblet']); // return boolean
 * PX.test('rwd', 'mobile'); // return boolean
 *
 * PX.test('media') // return false (it doesn't make sense)
 * PX.test('media', '(min-width: 800px)') // return boolean
 *
 * PX.test('tpl'); // return string (frontpage|article|...)
 * PX.test('tpl', ['article', 'diaporama', 'portfolio']); // return boolean
 * PX.test('tpl', 'homepage'); // return boolean
 *
 * PX.test('retina'); // return boolean
 * PX.test('touch'); // return boolean
 */

(function() {
	
	// Module declaration
	var moduleName = 'test',
		logPrefix = 'px.' + moduleName;
	
	if (!PX.mod.test(moduleName)) {
		return;
	}
	
	// Test unitaire en fonction du type
	var _unitTest = {
		
		// Responsive
		rwd: function (str) {
			if (str === _get.rwd()) {
				return true;
			}
			return false;
		},
		
		// Screen size
		screen: function (str) {
			if (str === _get.screen()) {
				return true;
			}
			return false;
		},
		
		// matchMedia
		media: (function(doc, docElem) {
			/* On test le support des media queries */
			var supportMQ,
			    refNode = docElem.firstElementChild || docElem.firstChild,
			    fakeBody = doc.createElement('body'),
			    div = doc.createElement('div');
			
			fakeBody.style.background = "none";
			div.id = "mq-syntax-test";
			div.style.cssText = "position:absolute;top:-100em";
			fakeBody.appendChild(div);
			div.innerHTML = "&shy;<style>@media screen and (min-width: 0px) { #mq-syntax-test { width: 12px; } }</style>";
			docElem.insertBefore(fakeBody, refNode);
			supportMQ = div.offsetWidth === 12;
			docElem.removeChild(fakeBody);
			
			return function (str) {
				// Media query non supporté => legacy IE
				if (!supportMQ) {
					return true;
				}
				return !!(window.matchMedia && window.matchMedia(str).matches);
			};
		}(document, document.documentElement)),
		
		// Template
		tpl: function (str) {
			if (PX.config.template && str === PX.config.template) {
				return true;
			}
			return false;
		},
		
		// Retina
		retina: function () {
			return window.devicePixelRatio && window.devicePixelRatio >= 1.5;
		},
		
		// Touch
		touch: function () {
			return !!('ontouchstart' in window) || !!(window.DocumentTouch && document instanceof DocumentTouch);
		}
	};
	
	// Getter en fonction du type
	var _get = {
		
		// Responsive break point
		rwd: function() {
			var style,
				breakpoint = 'desktop',
				regQuotes = /^('|")|('|")$/g;
			
			if (window.getComputedStyle) {
				style = window.getComputedStyle(document.documentElement, ":before");
	            if (style) {
	                breakpoint = style['content'] || null;
	                if (typeof breakpoint === 'string' || breakpoint instanceof String) {
	                    breakpoint = breakpoint.replace(regQuotes, "");
	                }
	            }
			}
			return breakpoint;
		},
		
		// Screen size
		screen: function() {
			var style,
				breakpoint = 'm',
				regQuotes = /^('|")|('|")$/g;
			
			if (window.getComputedStyle) {
				style = window.getComputedStyle(document.documentElement, ":after");
	            if (style) {
	                breakpoint = style['content'] || null;
	                if (typeof breakpoint === 'string' || breakpoint instanceof String) {
	                    breakpoint = breakpoint.replace(regQuotes, "");
	                }
	            }
			}
			return breakpoint;
		},
		
		// matchMedia => pas de getter
		media: function () {
			return;
		},
		
		// Template
		tpl: function () {
			return PX.config.template;
		},
		
		// Retina
		retina: _unitTest.retina,
		
		// Touch
		touch: _unitTest.touch
	};
	
	// Module definition
	PX[moduleName] = function (type, data) {
		var i,
			args = arguments.length;
		if (args === 1) {
			return _get[type]();
		}
		if (_unitTest[type]) {
			if (typeof data === 'string') {
				return _unitTest[type](data);
			}
			if (PXU.isArray(data) && data.length) {
				if (data.length === 1) {
					return _unitTest[type] && _unitTest[type](data[0]);
				}
				i = data.length;
				while (i--) {
					if (_unitTest[type](data[i])) {
						return true;
					}
				}
			}
		}
		return false;
	};	
})();

(function() {
	
	// Module declaration
	var moduleName = 'feature',
		logPrefix = 'px.' + moduleName;
	
	if (!PX.mod.test(moduleName)) {
		return;
	}
	
	function featureTest (property, value, noPrefixes) {
		// Thanks Modernizr! https://github.com/phistuck/Modernizr/commit/3fb7217f5f8274e2f11fe6cfeda7cfaf9948a1f5
		var prop = property + ':',
			el = document.createElement( 'test' ),
			mStyle = el.style;

		if( !noPrefixes ) {
			mStyle.cssText = prop + [ '-webkit-', '-moz-', '-ms-', '-o-', '' ].join( value + ';' + prop ) + value + ';';
		} else {
			mStyle.cssText = prop + value;
		}
		return mStyle[ property ].indexOf( value ) !== -1;
	}
	
	// Module definition
	PX[moduleName] = function (property, value, noPrefixes) {
		if (!property) {
			return false;
		}
		else {
			return featureTest (property, value, noPrefixes);
		}
	};
	
}());
