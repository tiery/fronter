<?php
include('../dev/bootstrap.php');
include('./assets/Parsedown.php');

// Get source
$source = file_get_contents('../readme.md');

// Convert it to HTML
$Parsedown = new Parsedown();
$html = $Parsedown->text($source);

// Templating
$output = theme_render_template('./assets/output.tpl.php', array('html' => $html));
print $output;
?>