<!doctype html>
<html>
<head>
	<title>Documentation</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="./assets/custom.css">
</head>
<body>
	
<div class="page">
	<?=$html?>
</div>

<script>
(function() {
	var pres = document.querySelectorAll('pre'), i = 0, l = pres.length;
	if (l) {
		for (; i<l; i++) {
			pres[i].className += ' prettyprint';
		}
	}
}());
</script>
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=desert&lang=css&lang=basic"></script>

</body>
</html>